#!/bin/bash

/app/bin/console cache:clear
/app/bin/console cache:warmup
chown -R :www-data var/sessions
chmod -R 775 var/sessions
chown -R :www-data var/log
chmod -R 775 var/log

php-fpm -D

nginx