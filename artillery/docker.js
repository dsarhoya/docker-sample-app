
  module.exports = {
    setBCookie: setBCookie,
    getAnswerId: getAnswerId
  }

  function setBCookie(context, events, done) {
    context.vars['b_cookie'] = makeid(20);
    return done();
  }

  function getAnswerId(requestParams, response, context, ee, next) {
    let location = response.req.path;
    let answerId = location.split('/')[3];
    context.vars['answerId'] = answerId;
    return next(); // MUST be called for the scenario to continue
  }
  
  function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 }