FROM dsarhoya/dsy-fpm-73-mariadb:2

RUN apt-get update \
	&& apt-get install -y \
	nginx \
	&& rm -rf /var/lib/apt/lists/*

COPY dockerbuild/dsy.ini $PHP_INI_DIR/conf.d
COPY dockerbuild/zdsy.conf /usr/local/etc/php-fpm.d

RUN rm /etc/nginx/sites-available/default
RUN rm /etc/nginx/sites-enabled/default
COPY dockerbuild/nginx.conf /etc/nginx/nginx.conf
ADD dockerbuild/dsy.conf /etc/nginx/sites-enabled/

RUN mkdir -p /var/log/php
RUN touch /var/log/php/error.log
RUN touch /var/log/php/slow.log

COPY . /app
WORKDIR /app

ADD dockerbuild/entrypoint.sh /entrypoint.sh
RUN ["chmod", "+x", "/entrypoint.sh"]
RUN echo "daemon off;" >> /etc/nginx/nginx.conf

ENTRYPOINT /entrypoint.sh