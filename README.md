## Docker ##

```
docker build -t dsarhoya/docker-sample-app:1 .
```

```
docker run --rm -p 8071:80 \
--env DB_HOST="host.docker.internal" \
--env SYMFONY_ENV=prod \
--name="dockerSampleApp" \
dsarhoya/docker-sample-app:7
```


docker run --rm -p 8071:80 \
--name="dockerSampleApp" \
--env DATABASE_URL="mysql://root:root@192.168.100.33:3305/docker?serverVersion=5.7" \
dsarhoya/docker-sample-app:1

docker run --rm -p 8071:80 \
--name="dockerSampleApp" \
--env DATABASE_URL="mysql://root:root@192.168.1.112:3306/docker?serverVersion=5.7" \
--env APP_ENV=prod \
--env APP_DEBUG=false \
dsarhoya/docker-sample-app:2